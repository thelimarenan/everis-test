import { Component, TemplateRef } from '@angular/core';
import { BsModalService} from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { PersonagensService } from './personagens/personagens.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  lista: any = []; 
  modalRef: BsModalRef;
  detalhesPersonagem: any = [];

  constructor(public service: PersonagensService, private modalService: BsModalService){
    this.getListaPersonagens();
  }

  getListaPersonagens(){
    this.service.getPersonagens().subscribe(data => {
      this.lista = data;
    })  
  }
  
  openModal(template: TemplateRef<any>, personagem) {
    this.detalhesPersonagem = personagem;
    this.modalRef = this.modalService.show(template);  
  }
}
