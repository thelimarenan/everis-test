import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ModalModule } from 'ngx-bootstrap/modal';

import { AppComponent } from './app.component';
import { PersonagensService } from './personagens/personagens.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ModalModule.forRoot()
  ],
  providers: [PersonagensService],
  bootstrap: [AppComponent]
})
export class AppModule { }
