import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class PersonagensService {

  private apiUrl = 'http://www.mocky.io/v2/5b2971722f00006300f561a9';

  constructor(public http: Http) {

  }

  getPersonagens(): Observable<string[]> {
    return this.http.get(this.apiUrl)
      .map(this.extractData)
      .catch(this.handleError)

  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      if (error.status === 500) {
        const body = error.json() || '';
        const err = "Erro na conexão com Servidor";
        alert(`${error.status} - ${err}`);
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw("");
  }

}
